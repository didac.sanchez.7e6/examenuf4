﻿using System;

namespace ExamenPokemon
{

    /*--- Interficies ---*/

    public interface IPokemon
    {
        public void Atacar(IPokemon obj);
        public void Curar();
    }

    /*--- Clases ---*/

    abstract public class Pokemon : IPokemon
    {

        /*--- Atrivuts ---*/
        public string Nombre { get; set; }
        public string Tipo { get; set; }
        public int Nivel { get; set; }
        public int VidaMaxima { get; set; }
        public int VidaActual { get; set; }

        /*--- Constructors ---*/

        public Pokemon(string nombre, string tipo, int nivel, int vidaMaxima, int vidaActual)
        {
            this.Nombre = nombre;
            this.Tipo = tipo;
            this.Nivel = nivel;
            this.VidaMaxima = vidaMaxima;
            this.VidaActual = vidaActual;
        }

        /*--- Metodos ---*/

        /*--- Metodos Hererados ---*/

        public void Atacar(IPokemon obj)
        {
            Console.WriteLine($"{Nombre} està atacando a {((Pokemon)obj).Nombre}");
            ((Pokemon)obj).VidaActual -= Nivel;
        }

        public void Curar()
        {
            VidaActual += 3;
            if (VidaActual > VidaMaxima) VidaActual = VidaMaxima;
            Console.WriteLine($"{Nombre} se esta curando");
        }

        /*--- Metodos Propios ---*/
        public void AtaquePoder(IPokemon poke)
        {
            if (Tipo.ToLower() == "agua") ((PokemonAgua)this).AtaquePoder(poke);
            if (Tipo.ToLower() == "fuego") ((PokemonFuego)this).AtaquePoder(poke);
            if (Tipo.ToLower() == "planta") ((PokemonPlanta)this).AtaquePoder(poke);
        }
    }
    public class PokemonAgua : Pokemon
    {
        /*--- Atrivuts ---*/
        public int PoderAgua { get; set; }

        /*--- Constructors ---*/
        public PokemonAgua(int poderAgua, string nombre, string tipo, int nivel, int vidaMaxima, int vidaActual) : base(nombre, tipo, nivel, vidaMaxima, vidaActual)
        {
            PoderAgua = poderAgua;
        }

        /*--- Metodos ---*/

        public void AtaquePoder(IPokemon poke)
        {
            Console.WriteLine($"{Nombre} ataca con poder de agua a {((Pokemon)poke).Nombre}");
            ((Pokemon)poke).VidaActual -= Nivel * PoderAgua;
        }
    }
    public class PokemonFuego : Pokemon
    {
        /*--- Atrivuts ---*/
        public int PoderFuego { get; set; }

        /*--- Constructors ---*/
        public PokemonFuego(int poderFuego, string nombre, string tipo, int nivel, int vidaMaxima, int vidaActual) : base(nombre, tipo, nivel, vidaMaxima, vidaActual)
        {
            PoderFuego = poderFuego;
        }

        /*--- Metodos ---*/

        public void AtaquePoder(IPokemon poke)
        {
            Console.WriteLine($"{Nombre} ataca con poder de fuego a {((Pokemon)poke).Nombre}");
            ((Pokemon)poke).VidaActual -= Nivel * PoderFuego;
        }
    }
    public class PokemonPlanta : Pokemon

    {
        /*--- Atrivuts ---*/
        public int PoderPlanta { get; set; }

        /*--- Constructors ---*/
        public PokemonPlanta(int poderPlanta, string nombre, string tipo, int nivel, int vidaMaxima, int vidaActual) : base(nombre, tipo, nivel, vidaMaxima, vidaActual)
        {
            PoderPlanta = poderPlanta;
        }

        /*--- Metodos ---*/

        public void AtaquePoder(IPokemon poke)
        {
            Console.WriteLine($"{Nombre} ataca con poder de planat a {((Pokemon)poke).Nombre}");
            ((Pokemon)poke).VidaActual -= Nivel * PoderPlanta;
        }
    }
    public class PruebasEstadioPokemon
    {
        public static void AtaqueElementa(IPokemon atacante, IPokemon defensor)
        {
            if (((Pokemon)atacante).Tipo.ToLower() == "agua" && ((Pokemon)defensor).Tipo.ToLower() == "fuego") ((Pokemon)atacante).AtaquePoder(defensor);
            else if (((Pokemon)atacante).Tipo.ToLower() == "fuego" && ((Pokemon)defensor).Tipo.ToLower() == "planta") ((Pokemon)atacante).AtaquePoder(defensor);
            else if (((Pokemon)atacante).Tipo.ToLower() == "planta" && ((Pokemon)defensor).Tipo.ToLower() == "agua") ((Pokemon)atacante).AtaquePoder(defensor);
            else
            {
                Console.WriteLine($"{((Pokemon)atacante).Nombre} ataca con poder de {((Pokemon)atacante).Tipo} a {((Pokemon)defensor).Nombre}, pero no hay bentaga elemental");
                ((Pokemon)atacante).Atacar(defensor);
            }
        }
        static void Main()
        {
            PokemonAgua mudkip = new PokemonAgua(2, "Mudkip", "Agua", 12, 35, 35);
            PokemonFuego litwick = new PokemonFuego(2, "Litwick", "Fuego", 16, 40, 40);
            PokemonPlanta leafeon = new PokemonPlanta(2, "Leafeon", "Planta", 20, 42, 42);

            /*--- Test 1 ---*/
            Batalla(litwick, leafeon);

            /*--- Test 2 ---*/
            Batalla(leafeon, leafeon);
        }
        public static int Menu(string write)
        {
            int action;
            do
            {
                Console.WriteLine(write);
            } while (!int.TryParse(Console.ReadLine(), out action));

            return action;
        }
        public static void Batalla(Pokemon prota, Pokemon pokemon)
        {
            do
            {
                Console.Clear();
                Console.WriteLine($"Tu Pokemon {prota.Nombre} tiene {prota.VidaActual} puntos de vida.\nEl Pokemon rival tiene {pokemon.VidaActual} puntos de vida");
                switch (Menu($"1) Atacar\n2) Ataque Elemental\n3) Curar\n4) Huir"))
                {
                    case 1:
                        prota.Atacar(pokemon);
                        break;
                    case 2:
                        AtaqueElementa(prota, pokemon);
                        break;
                    case 3:
                        prota.Curar();
                        continue;
                    case 4:
                        Console.WriteLine("Has huido");
                        pokemon.VidaActual = 0;
                        continue;
                    default:
                        break;
                }
                if (pokemon.VidaActual > 0) pokemon.Atacar(prota);
                if (pokemon.VidaActual <= 0)
                {
                    Console.WriteLine("Has guanyat");
                    break;
                }
                if (prota.VidaActual <= 0) Console.WriteLine("Has perdut");
                Console.ReadLine();
            } while (prota.VidaActual > 0 && pokemon.VidaActual > 0);
        }
    }
}
